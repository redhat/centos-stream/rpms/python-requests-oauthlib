#!/bin/bash -x

PKG="python-requests-oauthlib"

die () {
    echo "$1" >&2
    exit 1
}

#
# install dependencies (pytest & blinker)
#
rm -rf venv
python3 -m venv --system-site-packages --symlinks venv
. venv/bin/activate
pip install -r requirements-tests.txt

PKG="python-requests-oauthlib"
# If source not found, download it with dnf
if [ ! -d ./source ]; then
    # Extract source from srpm
    echo "## source directory not found.  attempting to download via rpm"
    dnf download --source ${PKG} && \
    rpm2cpio ${PKG}*.rpm|cpio -id && \
    mkdir source && \
    TARBALL="v$(rpm -q --qf="%{version}" -p ${PKG}*.rpm).tar.gz"
    tar -zxf ${TARBALL} -C source --strip-components=1
    if [ $? != 0 ]; then
        echo "Failed to download upstream tests"
        exit 1
    fi
fi

# rename source code directory to be sure that we are
# testing files from our rpm
cd source || die "Missing source directory!"

#
# run upstream tests, skip jwt tests
#
pytest -p no:warnings -k "not testCanPostBinaryData and not test_content_type_override and not test_url_is_native_str"| tee oauthlib.log
RESULT=${PIPESTATUS[0]}

#
#  cleanup
#

exit $RESULT
